package com.hellospring.demo.service;

import com.hellospring.demo.model.dto.ListMahasiswaDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface MahasiswaService {
    List<ListMahasiswaDto> getAllMahasiswa();
    ResponseEntity<Object> postMahasiswa(@RequestBody ListMahasiswaDto listMahasiswaDto);
}
