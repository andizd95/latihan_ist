package com.hellospring.demo.service;

import com.hellospring.demo.model.CustomMappingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQuery {

    @Autowired
    private EntityManager em;

    public List<CustomMappingModel> getCustomQueryNative(){
        String nativeQueryScript = "select id,name_course,name_mahasiswa,name_dosen from Dosen d \n" +
                "join Mahasiswa m on d.id=m.id_mahasiswa join mata_kuliah mk on m.id_mahasiswa=mk.id_mata_Kuliah " +
                "GROUP BY d.id,mk.name_course, m.name_mahasiswa,d.name_dosen;";
        Query q = em.createNativeQuery(nativeQueryScript, "QueryNativePakeJoin");


        return q.getResultList();
    }
}
