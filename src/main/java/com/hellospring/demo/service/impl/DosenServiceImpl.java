package com.hellospring.demo.service.impl;

import com.hellospring.demo.mapper.GetAllDosenMapper;
import com.hellospring.demo.mapper.PostDosenMapper;
import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.dto.DosenDto;
import com.hellospring.demo.model.dto.ListDosenDto;
import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.repository.DosenRepository;
import com.hellospring.demo.service.DosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DosenServiceImpl implements DosenService {


    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private PostDosenMapper postDosenMapper;

    @Autowired
    private GetAllDosenMapper getAllDosenMapper;


    @Override
    public List<ListDosenDto> getDataDosen() {

        List<Dosen> dosens = dosenRepository.findAll();
        List<ListDosenDto> listDosenDtos = getAllDosenMapper.mapperDosenDto(dosens);

        return listDosenDtos;
    }

    @Override
    public ResponseEntity<Object> postDosen(@RequestBody ListDosenDto listDosenDto) {
        Dosen dosen = postDosenMapper.dosenPostToEntity(listDosenDto);

        return new ResponseEntity<>(dosen, HttpStatus.OK);
    }

}
