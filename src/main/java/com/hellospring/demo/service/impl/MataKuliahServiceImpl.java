package com.hellospring.demo.service.impl;

import com.hellospring.demo.mapper.GetAllMataKuliahMapper;
import com.hellospring.demo.mapper.PostMataKuliahMapper;
import com.hellospring.demo.model.MataKuliah;
import com.hellospring.demo.model.dto.MataKuliahDto;
import com.hellospring.demo.repository.MataKuliahRepository;
import com.hellospring.demo.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MataKuliahServiceImpl implements MataKuliahService {

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private GetAllMataKuliahMapper getAllMataKuliahMapper;

    @Autowired
    private PostMataKuliahMapper postMataKuliahMapper;

    @Override
    public List<MataKuliahDto> getAllMataKuliah() {
       List<MataKuliah> mataKuliahs= mataKuliahRepository.findAll();
       List<MataKuliahDto> mataKuliahDtos= getAllMataKuliahMapper.mapperMataKuliahDto(mataKuliahs);

       return mataKuliahDtos;
    }

    @Override
    public ResponseEntity<Object> postMataKuliah(MataKuliahDto mataKuliahDto) {
        MataKuliah mataKuliah= postMataKuliahMapper.mataKuliiahPostToEntity(mataKuliahDto);

        return new ResponseEntity<>(mataKuliah, HttpStatus.OK);
    }


}
