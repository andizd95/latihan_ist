package com.hellospring.demo.repository;


import com.hellospring.demo.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa,Long> {
}
