package com.hellospring.demo.repository;

import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DosenRepository extends JpaRepository<Dosen,Long> {

}
