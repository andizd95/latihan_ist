package com.hellospring.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="mataKuliah")
public class MataKuliah {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mataKuliah")
    private Long id_mataKuliah;
    @Column(name = "name_course")
    private String name_course;

}
