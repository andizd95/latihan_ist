package com.hellospring.demo.model.dto;

import lombok.Data;

@Data
public class ListMahasiswaDto {
    private Long id_mahasiswa;
    private String name_mahasiswa;
    private String address_mahasiswa;
    private Long dosenid;
}
