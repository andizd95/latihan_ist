package com.hellospring.demo.model.dto;

import lombok.Data;

@Data
public class ListDosenDto {
    private Long id;
    private String name_dosen;
    private String address_dosen;
    private Long mataKuliah_id;
    private Long mahasiswa_id;
}
