package com.hellospring.demo.mapper;

import com.hellospring.demo.model.Mahasiswa;
import com.hellospring.demo.model.dto.ListMahasiswaDto;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllMahasiswaMapper {
    @Autowired
    private ModelMapperUtil modelMapperutil;

    public List<ListMahasiswaDto> mapperMahasiswaDto(List<Mahasiswa> mahasiswas){
        List<ListMahasiswaDto> mahasiswaDtos = new ArrayList<>();
        for (Mahasiswa data : mahasiswas){
            ListMahasiswaDto listMahasiswaDto = modelMapperutil.modelMapperUtil().map(data, ListMahasiswaDto.class);
            listMahasiswaDto.setId_mahasiswa(data.getId_mahasiswa());
            listMahasiswaDto.setName_mahasiswa(data.getName_mahasiswa());
            listMahasiswaDto.setAddress_mahasiswa(data.getAddress_mahasiswa());
            listMahasiswaDto.setDosenid(data.getId_mahasiswa());

            mahasiswaDtos.add(listMahasiswaDto);
        }
        return mahasiswaDtos;
    }
}
