package com.hellospring.demo.mapper;

import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.dto.ListDosenDto;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllDosenMapper {
    @Autowired
    private ModelMapperUtil modelMapperutil;

    public List<ListDosenDto> mapperDosenDto(List<Dosen> dosens){
        List<ListDosenDto> dosenDtos = new ArrayList<>();
        for (Dosen data : dosens){
            ListDosenDto listDosenDto = modelMapperutil.modelMapperUtil().map(data, ListDosenDto.class);
            listDosenDto.setId(data.getId());
            listDosenDto.setName_dosen(data.getName_dosen());
            listDosenDto.setAddress_dosen(data.getAddress_dosen());
            listDosenDto.setMataKuliah_id(data.getId());
            listDosenDto.setMahasiswa_id(data.getId());


            dosenDtos.add(listDosenDto);
        }
        return dosenDtos;
    }
}
