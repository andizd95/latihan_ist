package com.hellospring.demo.mapper;

import com.hellospring.demo.model.MataKuliah;
import com.hellospring.demo.model.dto.MataKuliahDto;
import com.hellospring.demo.repository.DosenRepository;
import com.hellospring.demo.repository.MataKuliahRepository;
import com.hellospring.demo.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PostMataKuliahMapper {
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private DosenRepository dosenRepository;

    public MataKuliah mataKuliiahPostToEntity(MataKuliahDto mataKuliahDto){
        MataKuliah mataKuliah = modelMapperUtil.modelMapperUtil().map(mataKuliahDto,MataKuliah.class);

        mataKuliah.setName_course(mataKuliah.getName_course());


        return mataKuliahRepository.save(mataKuliah);
    }
}
