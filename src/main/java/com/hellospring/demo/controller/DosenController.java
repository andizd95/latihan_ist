package com.hellospring.demo.controller;

import com.hellospring.demo.model.CustomMappingModel;
import com.hellospring.demo.model.Dosen;
import com.hellospring.demo.model.dto.ListDosenDto;
import com.hellospring.demo.repository.DosenRepository;
import com.hellospring.demo.service.CustomQuery;
import com.hellospring.demo.service.impl.DosenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class DosenController {
    @Autowired
    private DosenServiceImpl dosenServiceimpl;

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private CustomQuery ccDao;

    @GetMapping("/dosen")
    public List<ListDosenDto> getAllDosen(){

        return dosenServiceimpl.getDataDosen();

    }

    @GetMapping("/data")
    public ResponseEntity getData(){
        List<Dosen> result = dosenRepository.findAll();
        return ResponseEntity.ok(result);
    }


    @GetMapping("/nativeQuery")
    public ResponseEntity<List<CustomMappingModel>> getNativeQuery(){
        List<CustomMappingModel> list = ccDao.getCustomQueryNative();
        return ResponseEntity.ok(list);
    }

    @PostMapping("/dosen")
    public ResponseEntity<Object> postDosen(@RequestBody ListDosenDto listDosenDto){
        return dosenServiceimpl.postDosen(listDosenDto);
    }



}
